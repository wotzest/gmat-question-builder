# GMAT Question Builder

Platform to centralize all free questions from different sources, categorize them according to their tags and present them in a format similar to the real GMAT test.